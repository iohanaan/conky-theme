-- vim: ts=4 sw=4 noet ai cindent syntax=lua
--[[

]]

conky.config = {
    alignment = 'top_left',
    background = false,
    disable_auto_reload = true,
    border_width = 0,
    border_inner_margin = 0,
    border_outer_margin = 20,
    gap_x = 20,
    gap_y = 20,
    minimum_width = 240,
    minimum_height = 1040,
    maximum_width = 240,
    cpu_avg_samples = 2,
    default_color = 'D6D6D6',
    default_outline_color = 'white',
    default_shade_color = 'white',
    double_buffer = true,
    draw_borders = false,
    draw_graph_borders = true,
    draw_outline = false,
    draw_shades = false,
    use_xft = true,
    xftalpha = 0.8,
    font = 'Open Sans:size=9',
    text_buffer_size = 256,
    net_avg_samples = 2,
    no_buffers = true,
    out_to_console = false,
    out_to_stderr = false,
    override_utf8_locale = true,
    extra_newline = false,
    own_window = true,
    own_window_type = 'panel',
    own_window_hints = 'undecorated,sticky,skip_taskbar,skip_pager',
    own_window_colour = 'black',
    own_window_transparent = false,
    own_window_argb_visual = true,
    own_window_argb_value = 160,
    stippled_borders = 0,
    total_run_times = 0,
    update_interval = 1.0,
    uppercase = false,
    use_spacer = 'none',
    show_graph_scale = false,
    show_graph_range = false,
    if_up_strictness = 'link',
    temperature_unit = 'celsius',
}

local function interp (s, t)
    return s:gsub('(#%b{})', function (w)
        return t[w:sub(3, -2)] or w
    end)
end

conky.text = interp([[
#${alignc}${font Digital Readout Thick Upright:size=32}${time %H:%M}${font}
#${alignc}${time %A, %d %B, %Y}
#
SYSTEM ${voffset 2}${hr 2}
${goto 20}${if_match "${execi 1000 lsb_release -is}" == "Ubuntu"}\
${voffset 5}${font ConkyColorsLogos:size=16}u${font}${voffset -15}\
${else}\
${if_match "${execi 1000 lsb_release -is}" == "Debian"}\
${voffset 5}${font ConkyColorsLogos:size=24}d${font}${voffset -15}\
${else}\
${if_match "${execi 1000 lsb_release -is}" == "Fedora"}\
${voffset 5}${font ConkyColorsLogos:size=24}f${font}${voffset -15}\
${else}\
${if_match "${execi 1000 lsb_release -is}" == "Arch"}\
${voffset 5}${font ConkyColorsLogos:size=24}a${font}${voffset -15}\
${else}\
${voffset 5}${font ConkyColorsLogos:size=24}l${font}${voffset -22}\
${endif}\
${endif}\
${endif}\
${endif}\
${goto 55}${execi 1000 lsb_release -is} "${execi 1000 lsb_release -cs}" $machine
${goto 55}Kernel: ${alignr}${kernel}
${voffset 5}${font ConkyColors:size=16}b${font}\
${voffset -6}${goto 55}Host: ${alignr}${nodename}
${voffset 5}${font ConkyColors:size=16}o${font}\
${voffset -6}${goto 55}Uptime: ${alignr}${uptime}
${if_match "${acpiacadapter}" == "off-line"}\
${voffset 5}${font ConkyColors:size=16}1${font}\
${voffset -6}${goto 55}Battery: ${battery_percent BAT1}%\
${if_match ${battery_percent BAT1} < 15}${color red}${else}\
${if_match ${battery_percent BAT1} < 25}${color orange}${else}\
${if_match ${battery_percent BAT1} < 35}${color yellow}${else}\
${if_match ${battery_percent BAT1} < 90}${color white}${else}\
${color green}${endif}${endif}${endif}${endif}\
${alignr}${battery_bar 15,80 BAT1}${color}
${alignr}${battery_time BAT1}
${endif}\

CPU ${voffset 2}${hr 2}
${voffset -1}${if_match "${execi 1000 cat /proc/cpuinfo | grep 'vendor_id' | uniq | xargs | awk '{print $3}'}" == "GenuineIntel"}\
${font ConkyColorslogos:size=16}i${font}\
${else}\
${if_match "${execi 1000 cat /proc/cpuinfo | grep 'vendor_id' | uniq | xargs | awk '{print $3}'}" == "AuthenticAMD"}\
${font ConkyColorsLogos:size=16}r${font}\
${else}\
${font ConkyColors:size=16}d${font}\
${endif}\
${endif}\
${voffset -6}${goto 55}${execi 1000 cat /proc/cpuinfo | grep 'model name' | uniq | sed -e 's/model name.*: //' | cut -d ' ' -f1-3}
${voffset 5}${font ConkyColors:size=16}d${font}\
${voffset -6}${goto 55}Frequency: ${alignr}${freq_g} GHz
${voffset 5}${font ConkyColors:size=16}8${font}\
${voffset -6}${goto 55}Temperature: ${alignr}${exec ~/.conky/scripts/get_temp.sh coretemp-isa-0000}°C
${voffset 5}${font ConkyColors:size=16}l${font}\
${voffset -6}${goto 55}Usage: ${cpu}% ${alignr}${cpugraph 15,80}
#${voffset 5}${font ConkyColors:size=16}C${font}\
#${voffset -6}${goto 55}Fan Speed: ${alignr}${exec ~/.conky/scripts/fanspeed.sh} RPM

GPU ${voffset 2}${hr 2}
${if_match ${exec lsmod | grep 'nvidia' -c} > 0}\
${voffset -1}${font ConkyColorslogos:size=16}n${font}\
${voffset -6}${goto 55}${exec nvidia-smi --query-gpu=name --format=csv,noheader}
${voffset 5}${font ConkyColors:size=16}d${font}\
${voffset -6}${goto 55}Frequency: ${alignr}${exec nvidia-smi --query-gpu=clocks.current.graphics --format=csv,noheader}
${voffset 5}${font ConkyColors:size=16}8${font}\
${voffset -6}${goto 55}Temperature: ${alignr}${exec nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader,nounits}°C
#${voffset 5}${font ConkyColors:size=16}C${font}\
#${voffset -6}${goto 55}Fan Speed: ${alignr}${exec nvidia-smi --query-gpu=fan.speed --format=csv,noheader,nounits} RPM
${else}\
${voffset 5}${font ConkyColors:size=16}r${font}\
${voffset -6}${goto 55}OFF
${endif}\

GENERAL ${voffset 2}${hr 2}
${voffset 1}${font ConkyColors:size=16}g${font}\
${voffset -6}${goto 55}RAM: ${memperc}% ${alignr}${memgraph 15,80}
${if_match "${swapperc}" != "No swap"}\
${voffset 5}${font ConkyColors:size=16}i${font}\
${voffset -6}${goto 55}SWAP: ${swapperc}% ${alignr}${swapbar 15,80}\
${endif}
${voffset 5}${font ConkyColors:size=16}b${font}\
${voffset -6}${goto 55}Processes: ${alignr}${processes} (${running_processes} running)
${goto 55}Highest: ${alignr}CPU     RAM
${goto 55}${voffset -5}${hr 1}
${goto 55}${top name 1} ${alignr}${top cpu 1}  ${top mem 1}
${goto 55}${top name 2} ${alignr}${top cpu 2}  ${top mem 2}
${goto 55}${top name 3} ${alignr}${top cpu 3}  ${top mem 3}
${goto 55}${top name 4} ${alignr}${top cpu 4}  ${top mem 4}
${goto 55}${top name 5} ${alignr}${top cpu 5}  ${top mem 5}
${goto 55}${top name 6} ${alignr}${top cpu 6}  ${top mem 6}
${goto 55}${top name 7} ${alignr}${top cpu 7}  ${top mem 7}
${goto 55}${top name 8} ${alignr}${top cpu 8}  ${top mem 8}
${goto 55}${top name 9} ${alignr}${top cpu 9}  ${top mem 9}
${goto 55}${top name 10} ${alignr}${top cpu 10}  ${top mem 10}

DRIVES ${voffset 2}${hr 2}
#${voffset 1}${font ConkyColors:size=16}l${font}\
#${voffset -6}${goto 55}I/O: ${diskio} ${alignr}${diskiograph 15,80}
#${voffset 5}${font ConkyColors:size=16}8${font}\
#${voffset -6}${goto 55}SSD Temp: ${alignr}${exec ~/.conky/scripts/get_temp.sh nvme-pci-3c00}°C
#${voffset 5}${font ConkyColors:size=16}8${font}\
#${voffset -6}${goto 55}HDD Temp: ${alignr}${hddtemp /dev/sda}°C
${voffset 8}${font ConkyColors:size=16}i${font}\
${voffset -15}${goto 55}Root: ${alignr}${fs_bar 10,80 /}
${goto 55}${fs_used /} / ${fs_size /}
${voffset 12}${font ConkyColors:size=16}i${font}\
${voffset -15}${goto 55}Home: ${alignr}${fs_bar 10,80 /home}
${goto 55}${fs_used /home} / ${fs_size /home}
${if_existing #{storage}}\
${voffset 12}${font ConkyColors:size=16}i${font}\
${voffset -15}${goto 55}Storage: ${alignr}${fs_bar 10,80 #{storage}}
${goto 55}${fs_used #{storage}} / ${fs_size #{storage}}
${endif}\

NETWORK ${voffset 2}${hr 2}
${if_existing /proc/net/route #{wifi}}\
${voffset 8}${font ConkyColors:size=16}k${font}\
${voffset -15}${goto 55}SSID: ${alignr}${wireless_essid #{wifi}}
${goto 55}Signal: ${wireless_link_qual_perc #{wifi}}% ${alignr}${wireless_link_bar 10,80 #{wifi}}
${voffset 12}${font ConkyColors:size=16}s${font}\
${voffset -15}${goto 55}Up: ${upspeed #{wifi}}
${goto 55}Total: ${totalup #{wifi}} ${alignr}${voffset -18}${upspeedgraph #{wifi} 20,80}
${voffset 10}${font ConkyColors:size=16}t${font}\
${voffset -15}${goto 55}Down: ${downspeed #{wifi}}
${goto 55}Total: ${totaldown #{wifi}} ${alignr}${voffset -18}${downspeedgraph #{wifi} 20,80}
${voffset 10}${font ConkyColors:size=16}A${font}\
${voffset -15}${goto 55}Local: ${alignr}${addr #{wifi}}
${goto 55}Public: ${alignr}${execi 60 ~/.conky/scripts/ip.sh}
${else}${if_existing /proc/net/route #{eth}}\
${voffset 8}${font ConkyColors:size=16}s${font}\
${voffset -15}${goto 55}Up: ${upspeed #{eth}}
${goto 55}Total: ${totalup #{eth}} ${alignr}${voffset -18}${upspeedgraph #{eth} 20,80}
${voffset 10}${font ConkyColors:size=16}t${font}\
${voffset -15}${goto 55}Down: ${downspeed #{eth}}
${goto 55}Total: ${totaldown #{eth}} ${alignr}${voffset -18}${downspeedgraph #{eth} 20,80}
${voffset 10}${font ConkyColors:size=16}A${font}\
${voffset -15}${goto 55}Local: ${alignr}${addr #{eth}}
${goto 55}Public: ${alignr}${execi 60 ~/.conky/scripts/ip.sh}
${else}\
${voffset 1}${font ConkyColors:size=16}r${font}\
${voffset -6}${goto 55}Network Unavailable
${endif}\
${endif}\
]], {
storage = '/media/promobot/storage',
wifi = 'wlp62s0',
eth = 'enp0s31f6',
})
