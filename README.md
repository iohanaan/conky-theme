# Conky Theme

My theme for Conky

![alt text](image.png)

## Installation

### Ubuntu 16.04

Install `conky` and some depends.
```bash
$ sudo apt install conky lm-sensors hddtemp
```

Copy files:
* `.conkyrc` file to `~`
* `scripts` folder to `~/.conky`
* `fonts` folder to `~/.local/share`
* `start-conky.desktop` file to `~/.config/autostart`
