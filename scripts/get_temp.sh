#!/bin/bash

if [ $# -eq 0 ]; then
    echo "[N/A]"
    exit
fi
sensors -u "${1}" | grep "temp${2:-1}_input" | xargs | awk '{printf "%.f\n", $2}'
